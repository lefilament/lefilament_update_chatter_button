# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    'name': 'Chatter Le Filament',
    'category': 'Le Filament',
    'version': '10.0.1.0.0',
    'author': 'Le Filament',
    'summary': 'Update Chatter Button Le Filament',
    'website': 'http://www.le-filament.com',
    'license': 'AGPL-3',
    'depends': ['mail'],
    'data': [
        'views/webclient_templates.xml',
    ],
    'qweb': [
        "static/src/xml/*.xml",
    ],
    'installable': True,
    'auto_install': False,
    'bootstrap': True,
}
