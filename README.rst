.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


===================================
Le Filament - Update Chatter Button
===================================

Updates Mail module :
- log a note as default chatter button
- do not automatically add adressees of message as followers


Credits
=======

Contributors ------------

* Juliana Poudou <juliana@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
