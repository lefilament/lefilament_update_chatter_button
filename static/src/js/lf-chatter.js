// © 2018 Le Filament (<https://www.le-filament.com>)
// License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('override_chatter.override_chatter', function (require) {
"use strict";

var core = require('web.core');
var Chatter = require('mail.Chatter');
var MailThread = core.form_widget_registry.get('mail_thread');

var MailThreadOverride = MailThread.include({

    mute_new_message_button: function (mute) {
        if (mute) {
            this.$('.o_chatter_button_log_note').removeClass('btn-primary').addClass('btn-default');
        } else if (!mute) {
            this.$('.o_chatter_button_log_note').removeClass('btn-default').addClass('btn-primary');
        }
    },

});
});

